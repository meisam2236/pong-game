import pygame, sys, random

def ball_restart():
    global ball_speed_x, ball_speed_y
    ball.center = (screen_width/2, screen_height/2)
    ball_speed_y *= random.choice((1, -1))
    ball_speed_x *= random.choice((1, -1))

def ball_animation():
    global ball_speed_x, ball_speed_y
    ball.x += ball_speed_x
    ball.y += ball_speed_y

    if ball.top <= 0 or ball.bottom >= screen_height:
        ball_speed_y *= -1
    if ball.left <= 0 or ball.right >= screen_width:
        # game over
        ball_restart()
    if ball.colliderect(player) or ball.colliderect(oponent):
        ball_speed_x *= -1

def player_animation():
    player.y += player_speed
    if player.top <= 0:
        player.top = 0
    if player.bottom >= screen_height:
        player.bottom = screen_height

def oponent_animation():
    if oponent.top < ball.y:
        oponent.top += oponent_speed
    if oponent.bottom > ball.y:
        oponent.bottom -= oponent_speed
    if oponent.top <= 0:
        oponent.top = 0
    if oponent.bottom >= screen_height:
        player.bottom = screen_height

# General setup
pygame.init()
clock = pygame.time.Clock()

# Setting up the main window
screen_width = 1280
screen_height = 960
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Ping Pong')

# Game Rectangles
ball = pygame.Rect(int(screen_width/2) - 15, int(screen_height/2) - 15, 30, 30)
player = pygame.Rect(screen_width - 20, int(screen_height/2) - 70, 10, 140)
oponent = pygame.Rect(10, int(screen_height/2) - 70, 10, 140)
bg_color = pygame.Color('grey12')
light_gray = (200, 200, 200)

ball_speed_x = 7 * random.choice((1, -1))
ball_speed_y = 7 * random.choice((1, -1))
player_speed = 0
oponent_speed = 7

while True:
    # Handling input
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_DOWN:
                player_speed += 7
            if event.key == pygame.K_UP:
                player_speed -=7
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_DOWN:
                player_speed -= 7
            if event.key == pygame.K_UP:
                player_speed +=7

    ball_animation()
    player_animation()
    oponent_animation()
        
    # Visuals
    screen.fill(bg_color)
    pygame.draw.rect(screen, light_gray, player)
    pygame.draw.rect(screen, light_gray, oponent)
    pygame.draw.ellipse(screen, light_gray, ball)
    pygame.draw.aaline(screen, light_gray, (screen_width/2, 0), (screen_width/2, screen_height))
    # Updating the window
    pygame.display.flip()
    clock.tick(60)