# Pong Game 
This is a simple pong app... 
For running this you should have these installed: 
```bash
pip install pygame
``` 

To make an executable file first install pyinstaller and then run it in the file directory: 
```bash
pip install pyinstaller
pyinstaller pong.py --onefile --noconsole
```

